FROM openjdk:8-jdk-alpine
VOLUME /tmp
ADD ./api.products.jar app.jar
EXPOSE 8080
ENTRYPOINT ["java","-Dspring.data.mongodb.host=mongodb", "-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]
