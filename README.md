# myretail
A restful endpoint for retrieving product and price information

## How to Run:
The following commands assume you are in the cloned project's directory.

### Docker
The simplest way to run the endpoint is to use docker.
```
docker-compose up
```

### Individually
In order to run without docker you will have to set up your mongodb manually.
Mongo can be downloaded at https://www.mongodb.com/ with the community edition available [here](https://www.mongodb.com/download-center/community) at the time of this writing.

After mongo is running you can import an initial version of the data using the following command:
```
mongoimport --db productDB --collection prices --file src/main/resources/data/init.json --jsonArray
```

#### Maven
To run the project with maven as a new build, use the following command:
```
mvn spring-boot:run
```

if Mongo is not running on your localhost:
```
mvn spring-boot:run -Drun.arguments="--spring.data.mongodb.host=<hostname> --spring.data.mongdb.port=<port>"
```

#### Java
To run the project withe the pre built jar, use the following command:
```
java -jar ./api.products.jar
```

if Mongo is not running on your localhost:
```
java -jar ./api.products.jar --spring.data.mongodb.host=<hostname> --spring.data.mongdb.port=<port>
```

# Features

## Get Product Pricing Information
```
curl --request GET \
  --url http://localhost:8080/products/13860428 \
  --header 'accept: application/json'
```

## Update Product Pricing Information
```
curl --request PUT \
  --url http://localhost:8080/products/13860428 \
  --header 'content-type: application/json' \
  --data '{
    "id": 13860428,
    "name": "The Big Lebowski (Blu-ray)",
    "current_price": {
      "value": 9.99,
      "currency_code": "US"
    }
  }'
```
