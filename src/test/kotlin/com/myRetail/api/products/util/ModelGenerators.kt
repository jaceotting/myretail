package com.myRetail.api.products.util

import com.myRetail.api.products.dto.*
import com.myRetail.api.products.models.Price
import java.util.concurrent.ThreadLocalRandom

object ModelGenerators {
    private val random = ThreadLocalRandom.current()

    fun generateProductPriceDetail(productId: Int, name: String = random.nextString(), value: Double = random.nextPriceDouble(), currency: String = random.nextString()) = ProductPriceDetailsDTO(productId, name, PriceDetailsDTO(value, currency))

    fun generateProductPriceDetail(productId: Int, title: String = random.nextString(), priceDetail: PriceDetailsDTO?): ProductPriceDetailsDTO = ProductPriceDetailsDTO(id = productId, name = title, currentPrice = priceDetail)

    fun generateProductInformation(title: String = random.nextString()) = ProductInformationDTO(ProductDTO(ItemDTO(ProductDescriptionDTO(title))))

    fun generatePriceDetailDTO(value: Double = random.nextPriceDouble(), currency: String = random.nextString()) = PriceDetailsDTO(value, currency)

    fun generatePriceDetailDTO(price: Price) = PriceDetailsDTO(price.value, price.currency)

    fun generatePrice(productId: Int, value: Double = random.nextPriceDouble(), currency: String = random.nextString()) = Price(productId, value, currency)
}