package com.myRetail.api.products.util

import java.math.BigDecimal
import java.math.RoundingMode
import java.util.*
import java.util.concurrent.ThreadLocalRandom

fun ThreadLocalRandom.nextString() = UUID.randomUUID().toString()

fun ThreadLocalRandom.nextRoundedDouble(origin: Double, bound: Double): Double = BigDecimal(this.nextDouble(origin, bound).toString()).setScale(2, RoundingMode.HALF_UP).toDouble()

fun ThreadLocalRandom.nextPriceDouble() = this.nextRoundedDouble(0.0, 1000.0)
