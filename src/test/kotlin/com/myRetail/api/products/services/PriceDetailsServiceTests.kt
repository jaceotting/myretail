package com.myRetail.api.products.services

import com.myRetail.api.products.dto.PriceDetailsDTO
import com.myRetail.api.products.models.Price
import com.myRetail.api.products.repositories.PriceRepository
import com.myRetail.api.products.util.ModelGenerators.generatePrice
import com.myRetail.api.products.util.ModelGenerators.generatePriceDetailDTO
import com.myRetail.api.products.util.nextRoundedDouble
import io.mockk.*
import io.mockk.impl.annotations.InjectMockKs
import io.mockk.junit5.MockKExtension
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.catchThrowable
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import java.util.concurrent.ThreadLocalRandom

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ExtendWith(MockKExtension::class)
class PriceDetailsServiceTests {

    val random: ThreadLocalRandom = ThreadLocalRandom.current()

    private val priceRepository: PriceRepository = mockk()
    private val priceSlot = slot<Price>()

    @InjectMockKs
    var priceDetailsService = PriceDetailsService()

    @BeforeEach
    fun init() {
        clearMocks(
                priceRepository
        )

        every { priceRepository.save(capture(priceSlot)) } answers { priceSlot.captured }
    }

    @Nested
    inner class GetPriceDetails {
        @Test
        fun success() {
            // Given a valid product id
            val productId = random.nextInt()

            val expectedPrice = generatePrice(productId)
            val expectedPriceDetails = generatePriceDetailDTO(expectedPrice)
            every { priceRepository.findOneByProductId(productId) } returns expectedPrice

            // When get price details is called
            val result = priceDetailsService.getPriceDetails(productId)

            // Then the expected price details is returned
            assertThat(result).isEqualTo(expectedPriceDetails)
        }

        @Test
        fun `productId does not exist`() {
            // Given a product id that doesn't exist
            val productId = random.nextInt()

            every { priceRepository.findOneByProductId(productId) } returns null

            // When get price details is called
            val result = priceDetailsService.getPriceDetails(productId)

            // Then null is returned
            assertThat(result).isNull()
        }
    }

    @Nested
    inner class SavePriceDetails {
        @Test
        fun `update success`() {
            // Given a valid product id
            val productId = random.nextInt()

            val originalPrice = generatePrice(productId)
            val savedPrice = originalPrice.copy()
            every { priceRepository.findOneByProductId(productId) } returns savedPrice

            // And an updated priceDetails
            val updatedPriceDetails = PriceDetailsDTO(
                    value = originalPrice.value + 1.1,
                    currencyCode = originalPrice.currency + "S"
            )

            // When save price details is called
            priceDetailsService.savePriceDetails(productId, updatedPriceDetails)

            // Then the update is saved
            verify { priceRepository.save(any<Price>()) }
            val updatedPrice = priceSlot.captured

            assertThat(updatedPrice).isNotEqualTo(originalPrice)

            val expectedPrice = Price(productId, updatedPriceDetails.value, updatedPriceDetails.currencyCode)
            assertThat(updatedPrice).isEqualTo(expectedPrice)
        }

        @Test
        fun `create success`() {
            // Given a valid product id
            val productId = random.nextInt()

            // And a new priceDetails
            every { priceRepository.findOneByProductId(productId) } returns null
            val updatedPriceDetails = generatePriceDetailDTO()

            // When save price details is called
            priceDetailsService.savePriceDetails(productId, updatedPriceDetails)

            // Then the price details is saved
            verify { priceRepository.save(any<Price>()) }
            val updatedPrice = priceSlot.captured

            val expectedPrice = Price(productId, updatedPriceDetails.value, updatedPriceDetails.currencyCode)
            assertThat(updatedPrice).isEqualTo(expectedPrice)
        }

        @Test
        fun `value is set to negative`() {
            // Given a valid product id
            val productId = random.nextInt()

            // And a priceDetails with a negative value
            val updatedPriceDetails = generatePriceDetailDTO(value = random.nextRoundedDouble(-1000.0, -1.0))

            // When save price details is called
            val result = catchThrowable { priceDetailsService.savePriceDetails(productId, updatedPriceDetails) }

            // Then a NumberFormatException is thrown
            assertThat(result).isInstanceOf(NumberFormatException::class.java)

            // And nothing is saved
            verify(exactly = 0) { priceRepository.save(any<Price>()) }
        }

        @Test
        fun `value is in incorrect format`() {
            // Given a valid product id
            val productId = random.nextInt()

            // And an updated priceDetails with a bad format
            val updatedPriceDetails = generatePriceDetailDTO(value = random.nextRoundedDouble(0.0, 1000.0) + 0.1111111)

            // When save price details is called
            val result = catchThrowable { priceDetailsService.savePriceDetails(productId, updatedPriceDetails) }

            // Then a NumberFormatException is thrown
            assertThat(result).isInstanceOf(NumberFormatException::class.java)

            // And nothing is saved
            verify(exactly = 0) { priceRepository.save(any<Price>()) }
        }
    }

}
