package com.myRetail.api.products.services

import com.myRetail.api.products.dto.ProductInformationDTO
import com.myRetail.api.products.util.ModelGenerators.generateProductInformation
import io.mockk.clearMocks
import io.mockk.every
import io.mockk.impl.annotations.InjectMockKs
import io.mockk.junit5.MockKExtension
import io.mockk.mockk
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.catchThrowable
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.client.HttpClientErrorException
import org.springframework.web.client.RestTemplate
import java.net.URI
import java.util.concurrent.ThreadLocalRandom

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ExtendWith(MockKExtension::class)
class ProductInformationServiceTests {

    val random: ThreadLocalRandom = ThreadLocalRandom.current()

    val restTemplate: RestTemplate = mockk()

    @InjectMockKs(overrideValues = true, injectImmutable = true)
    var productInformationService = ProductInformationService()

    @BeforeEach
    fun init() {
        clearMocks(
                restTemplate
        )
    }

    @Nested
    inner class GetProductInformation {
        @Test
        fun success() {
            // Given a valid product id
            val productId = random.nextInt()

            val expectedProductInformation = generateProductInformation()
            val response = generateResponse(body = expectedProductInformation)
            every { restTemplate.getForEntity(any<URI>(), ProductInformationDTO::class.java) } returns response

            // When get product information is called
            val result = productInformationService.getProductInformation(productId)

            // Then the expected product information is returned
            assertThat(result).isEqualTo(expectedProductInformation)
        }

        @Test
        fun `not found status code returned`() {
            // Given a non-existant product id
            val productId = random.nextInt()

            val response = generateResponse(statusCode = HttpStatus.NOT_FOUND, body = null)
            every { restTemplate.getForEntity(any<URI>(), ProductInformationDTO::class.java) } returns response

            // When get product information is called
            val result = productInformationService.getProductInformation(productId)

            // Then null is returned
            assertThat(result).isNull()
        }

        @Test
        fun `unexpected status code returned`() {
            // Given a product id that generates an unexpected status code
            val productId = random.nextInt()

            val response = generateResponse(generateUnexpectedStatus())
            every { restTemplate.getForEntity(any<URI>(), ProductInformationDTO::class.java) } returns response

            // When get product information is called
            val result = catchThrowable { productInformationService.getProductInformation(productId) }

            // Then an HttpClientErrorException is thrown
            assertThat(result).isInstanceOf(HttpClientErrorException::class.java)
        }

        @Test
        fun `not found client error exception thrown`() {
            // Given a non-existant product id
            val productId = random.nextInt()

            val exception = HttpClientErrorException(HttpStatus.NOT_FOUND)
            every { restTemplate.getForEntity(any<URI>(), ProductInformationDTO::class.java) } throws exception

            // When get product information is called
            val result = productInformationService.getProductInformation(productId)

            // Then null is returned
            assertThat(result).isNull()
        }

        @Test
        fun `unexpected client error exception thrown`() {
            // Given a product id that generates an unexpected status code
            val productId = random.nextInt()

            val exception = HttpClientErrorException(generateUnexpectedStatus())
            every { restTemplate.getForEntity(any<URI>(), ProductInformationDTO::class.java) } throws exception

            // When get product information is called
            val result = catchThrowable { productInformationService.getProductInformation(productId) }

            // Then an HttpClientErrorException is thrown
            assertThat(result).isInstanceOf(HttpClientErrorException::class.java)
        }
    }

    private val expectedStatusCodes = arrayOf(HttpStatus.OK, HttpStatus.NOT_FOUND)
    fun generateUnexpectedStatus(): HttpStatus {
        return HttpStatus.values()
                .filterNot { expectedStatusCodes.contains(it) }
                .random()
    }

    fun generateResponse(statusCode: HttpStatus = HttpStatus.OK, body: ProductInformationDTO? = generateProductInformation()): ResponseEntity<ProductInformationDTO> {
        val response: ResponseEntity<ProductInformationDTO> = mockk()
        every { response.statusCode } returns statusCode
        every { response.body } returns body

        return response
    }
}
