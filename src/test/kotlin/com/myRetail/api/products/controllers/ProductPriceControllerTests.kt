package com.myRetail.api.products.controllers

import com.myRetail.api.products.dto.PriceDetailsDTO
import com.myRetail.api.products.services.PriceDetailsService
import com.myRetail.api.products.services.ProductInformationService
import com.myRetail.api.products.util.ModelGenerators.generatePriceDetailDTO
import com.myRetail.api.products.util.ModelGenerators.generateProductInformation
import com.myRetail.api.products.util.ModelGenerators.generateProductPriceDetail
import com.myRetail.api.products.util.nextString
import io.mockk.*
import io.mockk.impl.annotations.InjectMockKs
import io.mockk.junit5.MockKExtension
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.catchThrowable
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.http.HttpStatus
import org.springframework.web.client.HttpClientErrorException
import org.springframework.web.server.ResponseStatusException
import java.util.concurrent.ThreadLocalRandom

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ExtendWith(MockKExtension::class)
class ProductPriceControllerTests {

    val random: ThreadLocalRandom = ThreadLocalRandom.current()

    private val priceDetailsService: PriceDetailsService = mockk()
    private val priceDetailsSlot = slot<PriceDetailsDTO>()

    private val productInformationService: ProductInformationService = mockk()

    @InjectMockKs
    var productPriceController = ProductPriceController()

    @BeforeEach
    fun init() {
        clearMocks(
                priceDetailsService,
                productInformationService
        )

        every { priceDetailsService.savePriceDetails(any(), capture(priceDetailsSlot)) } answers { priceDetailsSlot.captured }
    }

    @Nested
    inner class GetProductPriceDetails {
        @Test
        fun success() {
            // Given a valid product id
            val productId = random.nextInt()

            val expectedTitle = random.nextString()
            val expectedProductInformation = generateProductInformation(expectedTitle)
            every { productInformationService.getProductInformation(productId) } returns expectedProductInformation

            val expectedPriceDetail = generatePriceDetailDTO()
            every { priceDetailsService.getPriceDetails(productId) } returns expectedPriceDetail

            // When the price details are retrieved
            val result = productPriceController.getProductPriceDetails(productId)

            // Then the result has the expected values
            val expectedResult = generateProductPriceDetail(productId, expectedTitle, expectedPriceDetail)
            assertThat(result).isEqualTo(expectedResult)
        }

        @Test
        fun `productId not in productInformationService`() {
            // Given a product id not in productInformationService
            val productId = random.nextInt()

            every { productInformationService.getProductInformation(productId) } returns null

            val expectedPriceDetail = generatePriceDetailDTO()
            every { priceDetailsService.getPriceDetails(productId) } returns expectedPriceDetail

            // When the price details are retrieved
            val result = catchThrowable { productPriceController.getProductPriceDetails(productId) } as ResponseStatusException

            // Then an exception with status "Not Found" is thrown
            assertThat(result.status).isEqualTo(HttpStatus.NOT_FOUND)
        }

        @Test
        fun `productInformationService throws an HttpClientErrorException`() {
            // Given a product id that causes a client error exception
            val productId = random.nextInt()

            val exception: HttpClientErrorException = mockk()
            every { productInformationService.getProductInformation(productId) } throws exception

            val expectedPriceDetail = generatePriceDetailDTO()
            every { priceDetailsService.getPriceDetails(productId) } returns expectedPriceDetail

            // When the price details are retrieved
            val result = catchThrowable { productPriceController.getProductPriceDetails(productId) } as ResponseStatusException

            // Then an exception with status "Internal Server Error" is thrown
            assertThat(result.status).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR)
        }

        @Test
        fun `productId not in priceRepository`() {
            // Given a product id not in priceRepository
            val productId = random.nextInt()

            val expectedTitle = random.nextString()
            val expectedProductInformation = generateProductInformation(expectedTitle)
            every { productInformationService.getProductInformation(productId) } returns expectedProductInformation

            every { priceDetailsService.getPriceDetails(productId) } returns null

            // When the price details are retrieved
            val result = productPriceController.getProductPriceDetails(productId)

            // Then the result has a null priceDetail
            val expectedResult = generateProductPriceDetail(productId, expectedTitle, priceDetail = null)
            assertThat(result).isEqualTo(expectedResult)
        }
    }

    @Nested
    inner class UpdateProductPriceDetails {
        @Test
        fun success() {
            // Given a valid product id
            val productId = random.nextInt()
            val originalProductPriceDetails = generateProductPriceDetail(productId, priceDetail = generatePriceDetailDTO())
            every { productInformationService.getProductInformation(productId) } returns generateProductInformation(originalProductPriceDetails.name!!)

            // And an updated priceDetail
            val originalPriceDetail = generatePriceDetailDTO()
            val updatedPriceDetail = PriceDetailsDTO(
                    originalPriceDetail.value + 1.0,
                    originalPriceDetail.currencyCode + "S"
            )

            val updatedProductPriceDetails = generateProductPriceDetail(originalProductPriceDetails.id!!, originalProductPriceDetails.name!!, updatedPriceDetail)

            // When update price details is called
            productPriceController.updateProductPriceDetails(productId, updatedProductPriceDetails)

            // Then save is called on the priceServices
            verify { priceDetailsService.savePriceDetails(productId, updatedPriceDetail) }
        }

        @Test
        fun `productId does not match provided productPriceDetail id`() {
            // Given a valid product id
            val productId = random.nextInt()
            val originalProductPriceDetails = generateProductPriceDetail(productId, priceDetail = generatePriceDetailDTO())
            every { productInformationService.getProductInformation(productId) } returns generateProductInformation(originalProductPriceDetails.name!!)

            // And an updated priceDetail
            val originalPriceDetail = generatePriceDetailDTO()
            val updatedPriceDetail = PriceDetailsDTO(
                    originalPriceDetail.value + 1.0,
                    originalPriceDetail.currencyCode + "S"
            )

            // And an updated productPriceDetail with a new product id
            val updatedProductPriceDetails = generateProductPriceDetail(originalProductPriceDetails.id!! + 1, originalProductPriceDetails.name!!, updatedPriceDetail)

            // When update price details is called
            val result = catchThrowable { productPriceController.updateProductPriceDetails(productId, updatedProductPriceDetails) } as ResponseStatusException

            // Then an exception with "Bad Request" is thrown
            assertThat(result).isNotNull()
            assertThat(result.status).isEqualTo(HttpStatus.BAD_REQUEST)
        }

        @Test
        fun `updated productPriceDetail name`() {
            // Given a valid product id
            val productId = random.nextInt()
            val originalProductPriceDetails = generateProductPriceDetail(productId, priceDetail = generatePriceDetailDTO())
            every { productInformationService.getProductInformation(productId) } returns generateProductInformation(originalProductPriceDetails.name!!)

            // And an updated priceDetail
            val originalPriceDetail = generatePriceDetailDTO()
            val updatedPriceDetail = PriceDetailsDTO(
                    originalPriceDetail.value + 1.0,
                    originalPriceDetail.currencyCode + "S"
            )

            // And an updated productPriceDetail with a new product name
            val updatedProductPriceDetails = generateProductPriceDetail(originalProductPriceDetails.id!!, random.nextString(), updatedPriceDetail)

            // When update price details is called
            val result = catchThrowable { productPriceController.updateProductPriceDetails(productId, updatedProductPriceDetails) } as ResponseStatusException

            // Then an exception with "Bad Request" is thrown
            assertThat(result).isNotNull()
            assertThat(result.status).isEqualTo(HttpStatus.BAD_REQUEST)
        }

        @Test
        fun `productId not in productInformationService`() {
            // Given a product id not in productInformationService
            val productId = random.nextInt()
            val originalProductPriceDetails = generateProductPriceDetail(productId, priceDetail = generatePriceDetailDTO())
            every { productInformationService.getProductInformation(productId) } returns null

            // And an updated priceDetail
            val originalPriceDetail = generatePriceDetailDTO()
            val updatedPriceDetail = PriceDetailsDTO(
                    originalPriceDetail.value + 1.0,
                    originalPriceDetail.currencyCode + "S"
            )

            val updatedProductPriceDetails = generateProductPriceDetail(originalProductPriceDetails.id!!, originalProductPriceDetails.name!!, updatedPriceDetail)

            // When update price details is called
            val result = catchThrowable { productPriceController.updateProductPriceDetails(productId, updatedProductPriceDetails) } as ResponseStatusException

            // Then an exception with status "Not Found" is thrown
            assertThat(result.status).isEqualTo(HttpStatus.NOT_FOUND)
        }

        @Test
        fun `productInformationService throws an HttpClientErrorException`() {
            // Given a product id that causes a client error exception
            val productId = random.nextInt()

            val originalProductPriceDetails = generateProductPriceDetail(productId, priceDetail = generatePriceDetailDTO())

            val exception: HttpClientErrorException = mockk()
            every { productInformationService.getProductInformation(productId) } throws exception

            // And an updated priceDetail
            val originalPriceDetail = generatePriceDetailDTO()
            val updatedPriceDetail = PriceDetailsDTO(
                    originalPriceDetail.value + 1.0,
                    originalPriceDetail.currencyCode + "S"
            )

            val updatedProductPriceDetails = generateProductPriceDetail(originalProductPriceDetails.id!!, originalProductPriceDetails.name!!, updatedPriceDetail)

            // When update price details is called
            val result = catchThrowable { productPriceController.updateProductPriceDetails(productId, updatedProductPriceDetails) } as ResponseStatusException

            // Then an exception with status "Internal Server Error" is thrown
            assertThat(result.status).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR)
        }

        @Test
        fun `priceService throws a NumberFormatException`() {
            // Given a valid product id
            val productId = random.nextInt()
            val originalProductPriceDetails = generateProductPriceDetail(productId, priceDetail = generatePriceDetailDTO())
            every { productInformationService.getProductInformation(productId) } returns generateProductInformation(originalProductPriceDetails.name!!)

            // And an updated priceDetail that throws a NumberFormatException
            val originalPriceDetail = generatePriceDetailDTO()
            val updatedPriceDetail = PriceDetailsDTO(
                    originalPriceDetail.value + 1.0,
                    originalPriceDetail.currencyCode + "S"
            )

            every { priceDetailsService.savePriceDetails(productId, updatedPriceDetail) } throws NumberFormatException()

            val updatedProductPriceDetails = generateProductPriceDetail(originalProductPriceDetails.id!!, originalProductPriceDetails.name!!, updatedPriceDetail)

            // When update price details is called
            val result = catchThrowable { productPriceController.updateProductPriceDetails(productId, updatedProductPriceDetails) } as ResponseStatusException

            // Then an exception with "Bad Request" is thrown
            assertThat(result).isNotNull()
            assertThat(result.status).isEqualTo(HttpStatus.BAD_REQUEST)
        }

        @Test
        fun `value of product price details object is null`() {
            // Given a valid product id
            val productId = random.nextInt()
            val originalProductPriceDetails = generateProductPriceDetail(productId, priceDetail = generatePriceDetailDTO())
            every { productInformationService.getProductInformation(productId) } returns generateProductInformation(originalProductPriceDetails.name!!)

            // And an updated priceDetail that is null
            val updatedPriceDetail = null

            val updatedProductPriceDetails = generateProductPriceDetail(originalProductPriceDetails.id!!, originalProductPriceDetails.name!!, updatedPriceDetail)

            // When update price details is called
            val result = catchThrowable { productPriceController.updateProductPriceDetails(productId, updatedProductPriceDetails) } as ResponseStatusException

            // Then an exception with "Bad Request" is thrown
            assertThat(result).isNotNull()
            assertThat(result.status).isEqualTo(HttpStatus.BAD_REQUEST)
        }
    }

}