package com.myRetail.api.products

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.myRetail.api.products.dto.ProductPriceDetailsDTO
import com.myRetail.api.products.models.Price
import com.myRetail.api.products.repositories.PriceRepository
import com.myRetail.api.products.util.ModelGenerators.generatePrice
import com.myRetail.api.products.util.ModelGenerators.generateProductPriceDetail
import com.myRetail.api.products.util.nextPriceDouble
import com.myRetail.api.products.util.nextString
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.http.*
import java.util.concurrent.ThreadLocalRandom

@Tag("integration-test")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class ProductPriceIT {

    private val random = ThreadLocalRandom.current()

    @Autowired
    lateinit var testRestTemplate: TestRestTemplate

    @Autowired
    lateinit var priceRepository: PriceRepository

    @AfterEach
    fun cleanupDB() {
        priceRepository.deleteAll()
    }

    @Nested
    inner class GetProductPriceDetails {
        @Test
        fun success() {
            // Given a product exists
            val existingProductPriceDetails = getExistingProductPriceDetail()
            savePrice(existingProductPriceDetails)

            // When get for the product id is called
            val result = testRestTemplate.getForEntity("/products/${existingProductPriceDetails.id!!}", ProductPriceDetailsDTO::class.java)

            // Then 200 is returned
            assertThat(result.statusCode).isEqualTo(HttpStatus.OK)
            // And the expected object is returned
            assertThat(result.body).isEqualTo(existingProductPriceDetails)
        }

        @Test
        fun `unknown product`() {
            // Given a product does not exist
            val nonExistantProductPriceDetails = generateProductPriceDetail(-13)
            savePrice(nonExistantProductPriceDetails)

            // When get for the product id is called
            val result = testRestTemplate.getForEntity("/products/${nonExistantProductPriceDetails.id!!}", ProductPriceDetailsDTO::class.java)

            // Then 404 is returned
            assertThat(result.statusCode).isEqualTo(HttpStatus.NOT_FOUND)
        }

        @Test
        fun `unknown price`() {
            // Given a product exists
            val existingProductPriceDetails = getExistingProductPriceDetail()

            // And its price does not
            priceRepository.findOneByProductId(existingProductPriceDetails.id!!)?.let {
                priceRepository.delete(it)
            }
            val existingPrice = priceRepository.findOneByProductId(existingProductPriceDetails.id!!)
            assertThat(existingPrice).isNull()

            // When get for the product id is called
            val result = testRestTemplate.getForEntity("/products/${existingProductPriceDetails.id!!}", ProductPriceDetailsDTO::class.java)

            // Then 200 is returned
            assertThat(result.statusCode).isEqualTo(HttpStatus.OK)

            // And the object is as expected, with a null current price
            val expectedProductPriceDetails = ProductPriceDetailsDTO(existingProductPriceDetails.id, existingProductPriceDetails.name, null)
            assertThat(result.body).isEqualTo(expectedProductPriceDetails)
        }

        @Test
        fun `unknown product and unknown price`() {
            // Given an unknown product
            val unknownProduct = generateProductPriceDetail(-44)

            // And its price is unknown
            val existingPrice = priceRepository.findOneByProductId(unknownProduct.id!!)
            assertThat(existingPrice).isNull()

            // When get for the product id is called
            val result = testRestTemplate.getForEntity("/products/${unknownProduct.id!!}", ProductPriceDetailsDTO::class.java)

            // Then 404 is returned
            assertThat(result.statusCode).isEqualTo(HttpStatus.NOT_FOUND)
        }
    }

    @Nested
    inner class PutProductPriceDetails {
        @Test
        fun success() {
            // Given a product that exists
            val existingProductPriceDetails = getExistingProductPriceDetail()
            val originalPrice = savePrice(existingProductPriceDetails).copy()

            // And a valid update for it
            val updatedPriceDetails = generateProductPriceDetail(
                    existingProductPriceDetails.id!!,
                    existingProductPriceDetails.name!!,
                    existingProductPriceDetails.currentPrice!!.value + 1.0,
                    random.nextString()
            )

            val json = jacksonObjectMapper().writeValueAsString(updatedPriceDetails)

            // When put for the product is called
            val response = testRestTemplate.putJsonString("/products/${existingProductPriceDetails.id!!}", json)

            // Then 200 is returned
            assertThat(response.statusCode).isEqualTo(HttpStatus.OK)

            // And the database object is updated
            val resultPrice = priceRepository.findOneByProductId(existingProductPriceDetails.id!!)!!
            assertThat(resultPrice.objectId).isEqualTo(originalPrice.objectId)

            assertThat(resultPrice.value).isNotEqualTo(originalPrice.value)
            assertThat(resultPrice.currency).isNotEqualTo(originalPrice.currency)

            assertThat(resultPrice.value).isEqualTo(updatedPriceDetails.currentPrice?.value)
            assertThat(resultPrice.currency).isEqualTo(updatedPriceDetails.currentPrice?.currencyCode)
        }

        @Nested
        inner class UpdateProductDetailsClientErrors {
            @Test
            fun `updated product id`() {
                // Given a product that exists
                val existingProductPriceDetails = getExistingProductPriceDetail()
                val originalPrice = savePrice(existingProductPriceDetails).copy()

                // And an update for it that changes the id
                val updatedPriceDetails = generateProductPriceDetail(
                        existingProductPriceDetails.id!! + 1,
                        existingProductPriceDetails.name!!,
                        existingProductPriceDetails.currentPrice!!.value + 1.0,
                        random.nextString()
                )

                val json = jacksonObjectMapper().writeValueAsString(updatedPriceDetails)

                // When put for the product is called
                val response = testRestTemplate.putJsonString("/products/${existingProductPriceDetails.id!!}", json)

                // Then 400 is returned
                assertThat(response.statusCode).isEqualTo(HttpStatus.BAD_REQUEST)

                // And the database object is not updated
                val resultPrice = priceRepository.findOneByProductId(existingProductPriceDetails.id!!)!!
                assertThat(resultPrice).isEqualTo(originalPrice)
            }

            @Test
            fun `updated product name`() {
                // Given a product that exists
                val existingProductPriceDetails = getExistingProductPriceDetail()
                val originalPrice = savePrice(existingProductPriceDetails).copy()

                // And an update for it that updates the name
                val updatedPriceDetails = generateProductPriceDetail(
                        existingProductPriceDetails.id!!,
                        random.nextString(),
                        existingProductPriceDetails.currentPrice!!.value + 1.0,
                        random.nextString()
                )

                val json = jacksonObjectMapper().writeValueAsString(updatedPriceDetails)

                // When put for the product is called
                val response = testRestTemplate.putJsonString("/products/${existingProductPriceDetails.id!!}", json)

                // Then 400 is returned
                assertThat(response.statusCode).isEqualTo(HttpStatus.BAD_REQUEST)

                // And the database object is not updated
                val resultPrice = priceRepository.findOneByProductId(existingProductPriceDetails.id!!)!!
                assertThat(resultPrice).isEqualTo(originalPrice)
            }
        }

        @Nested
        inner class UpdatePriceDetailsClientErrors {
            @Test
            fun `removed price details`() {
                // Given a product that exists
                val existingProductPriceDetails = getExistingProductPriceDetail()
                val originalPrice = savePrice(existingProductPriceDetails).copy()

                // And an update removes the price details
                val updatedPriceDetails = ProductPriceDetailsDTO(
                        existingProductPriceDetails.id!!,
                        existingProductPriceDetails.name!!,
                        null
                )

                val json = jacksonObjectMapper().writeValueAsString(updatedPriceDetails)

                // When put for the product is called
                val response = testRestTemplate.putJsonString("/products/${existingProductPriceDetails.id!!}", json)

                // Then 400 is returned
                assertThat(response.statusCode).isEqualTo(HttpStatus.BAD_REQUEST)

                // And the database object is not updated
                val resultPrice = priceRepository.findOneByProductId(existingProductPriceDetails.id!!)!!
                assertThat(resultPrice).isEqualTo(originalPrice)
            }

            @Test
            fun `removed price value`() {
                // Given a product that exists
                val existingProductPriceDetails = getExistingProductPriceDetail()
                val originalPrice = savePrice(existingProductPriceDetails).copy()

                // And an update removes the price value
                val updatedPriceDetails = generateProductPriceDetail(
                        existingProductPriceDetails.id!!,
                        existingProductPriceDetails.name!!,
                        -11111.0,
                        random.nextString()
                )

                val json = jacksonObjectMapper().writeValueAsString(updatedPriceDetails).replace("-11111.0", "null")

                // When put for the product is called
                val response = testRestTemplate.putJsonString("/products/${existingProductPriceDetails.id!!}", json)

                // Then 400 is returned
                assertThat(response.statusCode).isEqualTo(HttpStatus.BAD_REQUEST)

                // And the database object is not updated
                val resultPrice = priceRepository.findOneByProductId(existingProductPriceDetails.id!!)!!
                assertThat(resultPrice).isEqualTo(originalPrice)
            }

            @Test
            fun `removed currency code`() {
                // Given a product that exists
                val existingProductPriceDetails = getExistingProductPriceDetail()
                val originalPrice = savePrice(existingProductPriceDetails).copy()

                // And an update removes the currency code
                val updatedPriceDetails = generateProductPriceDetail(
                        existingProductPriceDetails.id!!,
                        existingProductPriceDetails.name!!,
                        existingProductPriceDetails.currentPrice!!.value + 1.0,
                        "REPLACE_AS_NULL"
                )

                val json = jacksonObjectMapper().writeValueAsString(updatedPriceDetails).replace("\"REPLACE_AS_NULL\"", "null")

                // When put for the product is called
                val response = testRestTemplate.putJsonString("/products/${existingProductPriceDetails.id!!}", json)

                // Then 400 is returned
                assertThat(response.statusCode).isEqualTo(HttpStatus.BAD_REQUEST)

                // And the database object is not updated
                val resultPrice = priceRepository.findOneByProductId(existingProductPriceDetails.id!!)!!
                assertThat(resultPrice).isEqualTo(originalPrice)
            }

            @Test
            fun `updated price value to negative`() {
                // Given a product that exists
                val existingProductPriceDetails = getExistingProductPriceDetail()
                val originalPrice = savePrice(existingProductPriceDetails).copy()

                // And an update for it that sets a negative price value
                val updatedPriceDetails = generateProductPriceDetail(
                        existingProductPriceDetails.id!!,
                        existingProductPriceDetails.name!!,
                        0 - (existingProductPriceDetails.currentPrice!!.value + 1.0),
                        random.nextString()
                )

                val json = jacksonObjectMapper().writeValueAsString(updatedPriceDetails)

                // When put for the product is called
                val response = testRestTemplate.putJsonString("/products/${existingProductPriceDetails.id!!}", json)

                // Then 400 is returned
                assertThat(response.statusCode).isEqualTo(HttpStatus.BAD_REQUEST)

                // And the database object is not updated
                val resultPrice = priceRepository.findOneByProductId(existingProductPriceDetails.id!!)!!
                assertThat(resultPrice).isEqualTo(originalPrice)
            }

            @Test
            fun `updated price value to non-price format`() {
                // Given a product that exists
                val existingProductPriceDetails = getExistingProductPriceDetail()
                val originalPrice = savePrice(existingProductPriceDetails).copy()

                // And an update for it that sets price to a bad format
                val updatedPriceDetails = generateProductPriceDetail(
                        existingProductPriceDetails.id!!,
                        existingProductPriceDetails.name!!,
                        existingProductPriceDetails.currentPrice!!.value + 1.111111,
                        random.nextString()
                )

                val json = jacksonObjectMapper().writeValueAsString(updatedPriceDetails)

                // When put for the product is called
                val response = testRestTemplate.putJsonString("/products/${existingProductPriceDetails.id!!}", json)

                // Then 400 is returned
                assertThat(response.statusCode).isEqualTo(HttpStatus.BAD_REQUEST)

                // And the database object is not updated
                val resultPrice = priceRepository.findOneByProductId(existingProductPriceDetails.id!!)!!
                assertThat(resultPrice).isEqualTo(originalPrice)
            }
        }

        @Test
        fun `unknown product`() {
            // Given a product does not exists in product information
            val nonExistantProductPriceDetails = generateProductPriceDetail(-13)
            val originalPrice = savePrice(nonExistantProductPriceDetails).copy()

            // And a valid update for it
            val updatedPriceDetails = generateProductPriceDetail(
                    nonExistantProductPriceDetails.id!!,
                    nonExistantProductPriceDetails.name!!,
                    nonExistantProductPriceDetails.currentPrice!!.value + 1.0,
                    random.nextString()
            )

            val json = jacksonObjectMapper().writeValueAsString(updatedPriceDetails)

            // When put for the product is called
            val response = testRestTemplate.putJsonString("/products/${nonExistantProductPriceDetails.id!!}", json)

            // Then 404 is returned
            assertThat(response.statusCode).isEqualTo(HttpStatus.NOT_FOUND)

            // And the database object is not updated
            val resultPrice = priceRepository.findOneByProductId(nonExistantProductPriceDetails.id!!)!!
            assertThat(resultPrice).isEqualTo(originalPrice)
        }

        @Test
        fun `unknown price`() {
            // Given a product that exists
            val existingProductPriceDetails = getExistingProductPriceDetail()

            // But we never saved its price
            val originalPrice = priceRepository.findOneByProductId(existingProductPriceDetails.id!!)
            assertThat(originalPrice).isNull()

            // And a valid update for it
            val updatedPriceDetails = generateProductPriceDetail(
                    existingProductPriceDetails.id!!,
                    existingProductPriceDetails.name!!,
                    existingProductPriceDetails.currentPrice!!.value + 1.0,
                    random.nextString()
            )

            val json = jacksonObjectMapper().writeValueAsString(updatedPriceDetails)

            // When put for the product is called
            val response = testRestTemplate.putJsonString("/products/${existingProductPriceDetails.id!!}", json)

            // Then 200 is returned
            assertThat(response.statusCode).isEqualTo(HttpStatus.OK)

            // And the database object is created
            val resultPrice = priceRepository.findOneByProductId(existingProductPriceDetails.id!!)!!
            assertThat(resultPrice.value).isEqualTo(updatedPriceDetails.currentPrice?.value)
            assertThat(resultPrice.currency).isEqualTo(updatedPriceDetails.currentPrice?.currencyCode)
        }
    }

    fun TestRestTemplate.putJsonString(url: String, json: String): ResponseEntity<Any> {
        val headers = HttpHeaders()
        headers.contentType = MediaType.APPLICATION_JSON
        val putEntity = HttpEntity(json, headers)
        return this.exchange(url, HttpMethod.PUT, putEntity, Any::class.java)
    }

    fun getExistingProductPriceDetail() = generateProductPriceDetail(13860428, "The Big Lebowski (Blu-ray)", 13.49, "USD")

    fun savePrice(productPriceDetails: ProductPriceDetailsDTO = generateProductPriceDetail(random.nextInt())): Price = savePrice(productPriceDetails.id!!, productPriceDetails.currentPrice?.value!!, productPriceDetails.currentPrice?.currencyCode!!)

    fun savePrice(productId: Int, value: Double = random.nextPriceDouble(), currency: String = random.nextString()): Price {
        return priceRepository.save(generatePrice(productId, value, currency))
    }

}
