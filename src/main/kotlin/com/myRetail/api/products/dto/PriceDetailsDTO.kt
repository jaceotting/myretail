package com.myRetail.api.products.dto

import com.fasterxml.jackson.annotation.JsonProperty
import com.myRetail.api.products.models.Price

data class PriceDetailsDTO (
        val value: Double,
        @JsonProperty("currency_code") val currencyCode: String
) {
    constructor(price: Price) : this(price.value, price.currency)
}