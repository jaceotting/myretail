package com.myRetail.api.products.dto

import com.fasterxml.jackson.annotation.JsonProperty

data class ProductInformationDTO(val product: ProductDTO?)

data class ProductDTO(val item: ItemDTO?)

data class ItemDTO(
        @JsonProperty("product_description") val productDescription: ProductDescriptionDTO?
)

data class ProductDescriptionDTO(val title: String?)
