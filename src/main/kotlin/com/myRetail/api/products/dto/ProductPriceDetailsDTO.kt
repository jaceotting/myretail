package com.myRetail.api.products.dto

import com.fasterxml.jackson.annotation.JsonProperty
import com.myRetail.api.products.dto.PriceDetailsDTO

data class ProductPriceDetailsDTO(
        val id: Int?,
        val name: String?,
        @JsonProperty("current_price") val currentPrice: PriceDetailsDTO?
)