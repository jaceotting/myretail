package com.myRetail.api.products.controllers

import com.myRetail.api.products.dto.ProductInformationDTO
import com.myRetail.api.products.dto.ProductPriceDetailsDTO
import com.myRetail.api.products.services.PriceDetailsService
import com.myRetail.api.products.services.ProductInformationService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.HttpClientErrorException
import org.springframework.web.server.ResponseStatusException

@RestController
@RequestMapping("/products")
class ProductPriceController {

    @Autowired lateinit var productInformationService: ProductInformationService
    @Autowired lateinit var priceDetailsService: PriceDetailsService

    @GetMapping("/{productId}", produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getProductPriceDetails(@PathVariable productId: Int): ProductPriceDetailsDTO {

        val productInformation = try {
            productInformationService.getProductInformation(productId) ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, "Product Information not found for Product Id: $productId")
        } catch (exception: HttpClientErrorException) {
            throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Error while retrieving product information for Product Id: $productId")
        }

        val productTitle = productInformation.product?.item?.productDescription?.title

        val priceDetails = priceDetailsService.getPriceDetails(productId)

        return ProductPriceDetailsDTO(
                id = productId,
                name = productTitle,
                currentPrice = priceDetails
        )
    }

    @PutMapping("/{productId}", consumes = [MediaType.APPLICATION_JSON_VALUE])
    fun updateProductPriceDetails(@PathVariable productId: Int, @RequestBody productPriceDetails: ProductPriceDetailsDTO): ResponseEntity<Unit> {
        if (productId != productPriceDetails.id ?: productId) {
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "ID of product is immutable")
        }

        val productInformation = try {
            productInformationService.getProductInformation(productId) ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, "Product Information not found for Product Id: $productId")
        } catch (exception: HttpClientErrorException) {
            throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Error while retrieving product information for Product Id: $productId")
        }

        if (!validateProductInformationUnchanged(productPriceDetails, productInformation)) {
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Non-price fields are immutable")
        }

        if (productPriceDetails.currentPrice == null) {
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "The current_price field must be set")
        }

        try {
            priceDetailsService.savePriceDetails(productId, productPriceDetails.currentPrice)
        } catch (exception: NumberFormatException) {
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Price was not in a valid format for Product Id: $productId")
        }

        return ResponseEntity(HttpStatus.OK)
    }

    private fun validateProductInformationUnchanged(productPriceDetails: ProductPriceDetailsDTO, productInformation: ProductInformationDTO) =
            productPriceDetails.name == productInformation.product?.item?.productDescription?.title
}