package com.myRetail.api.products.models

import org.bson.types.ObjectId
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.data.mongodb.core.mapping.Field

@Document(collection = "prices")
data class Price(
        var productId: Int,
        var value: Double,
        var currency: String,

        @Id @Field("_id") var objectId: ObjectId? = null
)