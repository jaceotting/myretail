package com.myRetail.api.products.repositories

import com.myRetail.api.products.models.Price
import org.springframework.data.mongodb.repository.MongoRepository

interface PriceRepository : MongoRepository<Price, String> {
    fun findOneByProductId(productId: Int): Price?
}