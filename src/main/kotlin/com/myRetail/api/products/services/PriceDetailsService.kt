package com.myRetail.api.products.services

import com.myRetail.api.products.dto.PriceDetailsDTO
import com.myRetail.api.products.models.Price
import com.myRetail.api.products.repositories.PriceRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.math.BigDecimal
import java.math.RoundingMode
import java.text.DecimalFormat

@Service
class PriceDetailsService {
    @Autowired
    lateinit var priceRepository: PriceRepository

    fun getPriceDetails(productId: Int): PriceDetailsDTO? {
        val price = priceRepository.findOneByProductId(productId)
        return price?.let {
            PriceDetailsDTO(
                value = price.value,
                currencyCode = price.currency
            )
        }
    }

    @Throws(NumberFormatException::class)
    fun savePriceDetails(productId: Int, priceDetails: PriceDetailsDTO): PriceDetailsDTO {
        // validate
        if (priceDetails.value < 0 || !validateDoubleAsPrice(priceDetails.value))
            throw NumberFormatException("value ${priceDetails.value} not in a valid price format")

        val updatedPrice = priceRepository.findOneByProductId(productId)?.let {
            it.value = priceDetails.value
            it.currency = priceDetails.currencyCode
            it
        } ?: Price(productId, priceDetails.value, priceDetails.currencyCode)

        val savedPrice = priceRepository.save(updatedPrice)

        return PriceDetailsDTO(savedPrice)
    }

    // Probably better to store or accept as major digits and minor digits and do work that way, but for simplicity's sake:
    // round to two digits and see if it's still the same value
    private fun validateDoubleAsPrice(value: Double): Boolean = BigDecimal(value.toString()).setScale(2, RoundingMode.HALF_UP).toDouble() == value
}