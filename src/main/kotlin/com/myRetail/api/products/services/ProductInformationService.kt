package com.myRetail.api.products.services

import com.myRetail.api.products.dto.ProductInformationDTO
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.client.HttpClientErrorException
import org.springframework.web.client.RestTemplate
import org.springframework.web.util.UriComponentsBuilder
import java.net.URI

object ProductInformationServiceConstants {
    const val URI_SCHEME = "https"
    const val URI_HOST = "redsky.target.com"
    const val URI_PATH = "/v2/pdp/tcin/{productId}"
    const val URI_QUERY = "excludes=taxonomy,price,promotion,bulk_ship,rating_and_review_reviews,rating_and_review_statistics,question_answer_statistics"
}

@Service
class ProductInformationService {

    val restTemplate = RestTemplate()

    @Throws(HttpClientErrorException::class)
    fun getProductInformation(productId: Int): ProductInformationDTO? {
        val response = try {
            restTemplate.getForEntity(buildRequestUri(productId), ProductInformationDTO::class.java)
        } catch (exception: HttpClientErrorException) {
            if (exception.statusCode == HttpStatus.NOT_FOUND) {
                return null
            }
            else {
                throw exception
            }
        }

        return when(response.statusCode) {
            HttpStatus.OK -> response.body
            HttpStatus.NOT_FOUND -> null
            else -> {
                throw HttpClientErrorException(response.statusCode)
            }
        }
    }

    private fun buildRequestUri(productId: Int): URI = UriComponentsBuilder
            .newInstance()
            .scheme(ProductInformationServiceConstants.URI_SCHEME)
            .host(ProductInformationServiceConstants.URI_HOST)
            .path(ProductInformationServiceConstants.URI_PATH)
            .query(ProductInformationServiceConstants.URI_QUERY)
            .buildAndExpand(productId)
            .toUri()
}

